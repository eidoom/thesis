#include <algorithm>
#include <array>
#include <cmath>
#include <complex>
#include <iomanip>
#include <iostream>

#include "analytic/0q4g-analytic.h"
#include "analytic/0q5g-analytic.h"
#include "ir/soft_gg2g-analytic.h"
#include "ngluon2/Mom.h"

int main()
{
    // massless 5-point phase space with full_mom[3] soft
    const std::array<MOM<double>, 5> full_mom { {
        { -5.0000000000000000e-01, 0.0000000000000000e+00, 0.0000000000000000e+00, -5.0000000000000000e-01 },
        { -5.0000000000000000e-01, 0.0000000000000000e+00, 0.0000000000000000e+00, 5.0000000000000000e-01 },
        { 4.9999949999999999e-01, 4.3301226887951738e-01, 0.0000000000000000e+00, 2.4999975000000005e-01 },
        { 4.9999999999772449e-06, -2.2505887718767004e-06, 1.7633487034433740e-06, -4.1018839000804920e-06 },
        { 4.9999550000000009e-01, -4.3301001829074548e-01, -1.7633487034433740e-06, -2.4999564811609998e-01 },
    } };

    // initialise amplitudes
    Amp0q5g_a<double> full_amp;
    Amp0q4g_a<double> reduced_amp;
    Softgg2g_a<double> soft_amp;

    // set momenta
    full_amp.setMomenta(full_mom.data());

    // helicities for amplitudes
    const std::array<int, 5> full_hels { +1, +1, -1, -1, -1 };
    const std::array<int, 4> reduced_hels { +1, +1, -1, -1 };
    // the eikonal amplitude is independent of the helicities of the correlated legs
    const std::array<int, 3> soft_hels { 0, -1, 0 };

    // perform reduced colour sum
    // indices i,3,k are in the full 5-point phase space
    // indices ii,kk are in the reduced 4-point phase space
    double lim {};
    int ref_index {};
    for (int i { 0 }; i < 5; ++i) {
        if (i != 3) {
            const int ii { i < 3 ? i : (i - 1) % 4 };

            for (int k { i + 1 }; k < 5; ++k) {
                if (k != 3) {
                    const int kk { k < 3 ? k : (k - 1) % 4 };

                    // reduced phase space is just the full phase space without the soft leg
                    std::array<MOM<double>, 4> reduced_mom {};
                    std::copy_n(full_mom.cbegin(), 3, reduced_mom.begin());
                    reduced_mom[3] = full_mom[4];

                    // ensure physical reduced phase space
                    const double b { dot(full_mom[i], full_mom[3]) / (dot(full_mom[i], full_mom[k]) + dot(full_mom[3], full_mom[k])) };
                    reduced_mom[ii] = full_mom[i] + full_mom[3] - b * full_mom[k];
                    reduced_mom[kk] = (1. + b) * full_mom[k];

                    reduced_amp.setMomenta(reduced_mom.data());

                    // ensure reference momentum is not the same as another momentum in use
                    // to avoid division by zero from brackets in the denominator of the form < i i >
                    while ((ref_index == i) || (ref_index == 3) || (ref_index == k)) {
                        ref_index = (ref_index + 1) % 5;
                    }

                    // set momenta for eikonal amplitude
                    // the second argument is the reference momentum
                    soft_amp.setMomenta({ reduced_mom[ii], full_mom[3], reduced_mom[kk] }, full_mom[ref_index]);

                    // compute eikonal and colour-correlated reduced matrix elements
                    const double soft_val { soft_amp.born(soft_hels.data()) },
                        cc_val { reduced_amp.born_ccij(reduced_hels.data(), ii, kk) };

                    lim += cc_val * soft_val;
                }
            }
        }
    }

    // we summed over upper triangle of colour correlation matrix
    // diagonals receive no contribution because eikonal kinematic factor is zero due to antisymmetry of brackets
    // matrix is symmetric, so add lower triangle by doubling result
    lim *= 2.;

    // compute full matrix element
    const double amp_val { full_amp.born(full_hels.data()) };

    // print results (with unity indexing)
    std::cout
        << std::setprecision(1) << std::scientific
        << '\n'
        << std::setw(25) << "|{E_4}^2/s_{12}| = "
        << std::setw(7) << std::abs(pow(full_mom[3].x0, 2) / dot(full_mom[0], full_mom[1])) << '\n'
        << std::setw(25) << "|(amp^2-lim^2)/amp^2| = "
        << std::setw(7) << std::abs((amp_val - lim) / amp_val) << '\n'
        << '\n';
}
