% General packages
\usepackage[utf8]{inputenc} % utf8 input good for non-ascii characters
\usepackage[T1]{fontenc} % Good for hyphenating non-ascii characters
\usepackage{lmodern}
\usepackage[british]{babel}
\usepackage[nottoc]{tocbibind} % Includes bibliography in table of contents, but not the table of contents itself
\usepackage[figuresright]{rotating} % Allows for sideways figures and tables
\usepackage{exscale}
\usepackage{relsize}

\interfootnotelinepenalty=10000

\usepackage{xcolor}
\definecolor{solarized-base3}{RGB}{253, 246, 227}

\usepackage{minted} % syntax highlighting and formatting for code blocks
\usemintedstyle{solarized-light}
\setminted{
    breaklines=true,
    breakindentnchars=4,
    linenos=true,
    fontsize=\small,
    bgcolor=solarized-base3,
}

\usepackage{cite} % Combine multiple citations in range

% Nicer appearance / typesetting
\usepackage[shrink=10,stretch=10]{microtype}
% We define a command to disable and enable protrusion, used for the tables of contents
\makeatletter
\@ifpackageloaded{microtype}{%
	\providecommand{\disableprotrusion}{\microtypesetup{protrusion=false}}
	\providecommand{\enableprotrusion}{\microtypesetup{protrusion=true}}
}{%
	\providecommand{\disableprotrusion}{}
	\providecommand{\enableprotrusion}{}
}
\makeatother

\usepackage{booktabs} % Better tables

% These make paragraphs not indented, and separate consecutive paragraphs
% \setlength{\parindent}{0pt}
% \setlength{\parskip}{0.5em plus 3pt minus 3pt}

% Format captions
% \usepackage[margin=15mm,hang]{caption}
\usepackage[margin=2em]{caption}
% \usepackage{caption}
% \captionsetup[figure]{labelfont=bf}
\usepackage{subcaption}
\usepackage{perpage} % Allows to reset counters on each page
\MakePerPage{footnote} % Resets footnote counters on each page

\graphicspath{{img/}}

% In align*, use this to number a particular line
% Rather than using align, and \nonumber-ing every other line
\newcommand\numberthis{\addtocounter{equation}{1}\tag{\theequation}}

% Provide itemize and enumerate without extra spacing
\newenvironment{itemize*}
{\begin{itemize}
	\setlength{\itemsep}{0pt}
	\setlength{\parskip}{0pt}}
{\end{itemize}}
\newenvironment{enumerate*}
{\begin{enumerate}
	\setlength{\itemsep}{0pt}
	\setlength{\parskip}{0pt}}
{\end{enumerate}}

% Better maths support
\usepackage{amsmath,amssymb,mathtools}
\numberwithin{equation}{section}
\allowdisplaybreaks % Allow display equations to break over different pages
\usepackage{amssymb}
% \usepackage{bm} % More bold maths symbols
\usepackage{bbm} % More blackboard bold characters, via \mathbbm. Mostly for blackboard bold 1
\usepackage{xfrac} % Nice small fractions
\usepackage{array} % Allows us to define custom column types for tables
\newcolumntype{L}{>{$}l<{$}} % a left aligned maths column type

% Some useful physics packages
\usepackage[italic]{hepnames} % Add particle name macros (e.g. \PBs)
\usepackage{braket} % Adds Dirac bra-ket notation
\usepackage{slashed} % Adds Dirac slash notation

\usepackage{siunitx}[=v2] % 2022-02-21: Fedora 35 doesn't have v3
\sisetup{
	separate-uncertainty, % uncertainties with +- symbol, 
% 	range-phrase = --, % ranges with dash
% 	range-units = single % only write unit once
} 
\DeclareSIUnit\fb{\femto\barn}
\DeclareSIUnit\pb{\pico\barn}

\usepackage{physics}
\usepackage{xspace}
\usepackage{multirow}
\usepackage{mleftright}
\usepackage[printonlyused,withpage]{acronym}
\usepackage{imakeidx}

\makeindex[columns=2,intoc]

\newcommand{\textbi}[1]{\textit{\textbf{#1}}}

\newcommand{\ie}{\textit{i.e.}\xspace}
\newcommand{\cf}{\textit{c.f.}\xspace}
\newcommand{\eg}{\textit{e.g.}\xspace}

\newcommand{\br}[1]{\left( #1 \right)}
\newcommand{\sq}[1]{\left[ #1 \right]}
\newcommand{\cu}[1]{\left\{ #1 \right\}}
\newcommand{\ab}[1]{\left| #1 \right|}
\newcommand{\msq}[1]{\ab{#1}^2}

\newcommand{\brf}[1]{\mleft( #1 \mright)}
\newcommand{\brm}[1]{\begin{pmatrix} #1 \end{pmatrix}}
\newcommand{\brsm}[1]{\begin{psmallmatrix} #1 \end{psmallmatrix}}

\newcommand{\sqf}[1]{\mleft[ #1 \mright]}

\newcommand{\cum}[1]{\begin{Bmatrix} #1 \end{Bmatrix}}

\newcommand{\aem}{{\alpha}}
\newcommand{\astr}{{\alpha_\mathrm{s}}}
\newcommand{\cs}{{\mathrm{g}_\mathrm{s}}}
\newcommand{\cem}{{\mathrm{e}}}
\newcommand{\fc}{{\mathrm{Q}_\mathrm{q}}}
\newcommand{\am}{\mathcal{A}}
\newcommand{\as}{\ab{\mathcal{A}}^2}
\newcommand{\lls}[3]{{{#1}^{\mathrm{(#2)}}_{#3}}}
\newcommand{\amp}[2]{\lls{\am}{#1}{#2}}
\newcommand{\ant}{\amp{0}{n}}
\newcommand{\pa}{A}
\newcommand{\pamp}[2]{\lls{\pa}{#1}{#2}}
\newcommand{\pat}[2]{\pa_{#2}^{\mathrm{(0)}}\brf{#1}}
\newcommand{\pant}[1]{\pat{#1}{n}}
\newcommand{\realamplitude}{\amplitude_{\mathrm{R}}}
\newcommand{\virtualamplitude}{\amplitude_{\mathrm{V}}}
\newcommand{\nc}{{N_{c}}}
\newcommand{\nf}{{N_{f}}}
\newcommand{\gU}[1]{\mathrm{U}(#1)}
\newcommand{\SU}[1]{\mathrm{S}\gU{#1}}
\newcommand{\SUt}[1]{$\SU{#1}$}
\newcommand{\cgf}{t}
\newcommand{\cga}{F}
\newcommand{\cdi}{{\mathrm{T}_\mathrm{F}}}
\newcommand{\casimir}{\mathrm{C}}
\newcommand{\caf}{{\casimir_\mathrm{F}}}
\newcommand{\caa}{{\casimir_\mathrm{A}}}
\newcommand{\spindimension}{{d_s}}
\newcommand{\cof}{c}
\newcommand{\crosssection}{\sigma}
\newcommand{\rcs}{\crosssection_{\mathrm{R}}}
\newcommand{\vcs}{\crosssection_{\mathrm{V}}}
\newcommand{\sigmaLO}{\crosssection_\mathrm{LO}}
\newcommand{\sigmaNLO}{\crosssection^{\mathrm{(NLO)}}}
\newcommand{\cm}{\mathcal{C}}
\newcommand{\gct}{\mathcal{I}}
\newcommand{\lct}{\dd{\mathcal{D}}}
\newcommand{\go}{\mathcal{O}}
\newcommand{\cps}{\dd{\Phi_{4\parallel5}}}
\newcommand{\explicitphasespace}[3]{#1\brf{#2 \rightarrow #3}}
\newcommand{\bpsf}{\dd{{\Phi}_2}}
\newcommand{\pcps}[2]{\explicitphasespace{\cps}{#1}{#2}}
\newcommand{\pbpsf}[2]{\explicitphasespace{\bpsf}{#1}{#2}}
\newcommand{\ff}{F}
\newcommand{\stc}{f}
\newcommand{\covcon}[3]{{#1}_{#2}^{\phantom{#2}#3}}
\newcommand{\concov}[3]{{#1}^{#2}_{\phantom{#2}#3}}
\newcommand{\lws}{\lambda}
\newcommand{\rws}{\widetilde{\lws}}
\newcommand{\lwsu}[2]{\covcon{\lws}{#1}{\dot{#2}}}
\newcommand{\lwsug}[1]{\lws^{\dot{#1}}}
\newcommand{\rwsu}[2]{\covcon{\rws}{#1}{#2}}
\newcommand{\rwsug}[1]{\rws^{#1}}
\newcommand{\lwsd}[2]{{\br{\lws_{#1}}}_{\dot{#2}}}
\newcommand{\lwsdg}[1]{\lws_{\dot{#1}}}
\newcommand{\rwsd}[2]{\br{\rws_{#1}}_{#2}}
\newcommand{\rwsdg}[1]{\rws_{#1}}
\newcommand{\lc}{\varepsilon}
\newcommand{\llc}[3]{\lc#1{\dot{#2}\dot{#3}}}
\newcommand{\llcu}[2]{\llc{^}{#1}{#2}}
\newcommand{\llcd}[2]{\llc{_}{#1}{#2}}
\newcommand{\rlc}[3]{\lc#1{#2#3}}
\newcommand{\rlcu}[2]{\rlc{^}{#1}{#2}}
\newcommand{\rlcd}[2]{\rlc{_}{#1}{#2}}
\newcommand{\ptr}[1]{\tr(#1)}
\newcommand{\ptrs}[2]{\tr_{#1}(#2)}
\newcommand{\ptrf}[1]{\ptrs{5}{#1}}
\newcommand{\pdet}[1]{\det(#1)}
\newcommand{\imi}{{\mathrm{i}\mkern1mu}}
\newcommand{\s}[1]{{{s}_{#1}}}
\newcommand{\sh}[1]{{\,\hat{\text{s}}_{#1}}}
\newcommand{\trf}{\text{tr}_5}
\newcommand{\sumsquare}[1]{\overline{\ab{#1}^2}}
\newcommand{\deltaplus}[1]{\delta^{(+)}\brf{#1}}
\newcommand{\nth}[1]{{#1}^{\mathrm{th}}}
\newcommand{\pov}{\varepsilon}
\newcommand{\cn}{\mathbb{C}}
\newcommand{\integers}{\mathbb{Z}}
\newcommand{\nonnegativeintegers}{\integers^{\ge}}
\newcommand{\positiveintegers}{\integers^{>}}
\newcommand{\oddintegers}{\integers^\text{odd}}
\newcommand{\evenintegers}{\integers^\text{even}}
\newcommand{\rationals}{\mathbb{Q}}
\newcommand{\ffield}[1]{{\mathbb{F}_{#1}}}
\newcommand{\collinearlimit}[2]{$#1\!\parallel\!#2$}
\newcommand{\cl}{\collinearlimit}
\newcommand{\rmat}[1]{\begin{pmatrix}#1\end{pmatrix}}
\newcommand{\lm}{\eta}
\newcommand{\sigmabar}{\overline{\sigma}}
\newcommand{\pmin}[1]{\min\brf{#1}}
\newcommand{\pmax}[1]{\max\brf{#1}}

\newcommand{\code}[1]{\texttt{#1}\xspace}
\newcommand{\python}{\code{Python}}
\newcommand{\cpp}{\code{C++}}
\newcommand{\njet}{\code{NJet3}}
\newcommand{\eigen}{\code{Eigen3}}
\newcommand{\openloops}{\code{OpenLoops2}}
\newcommand{\nnlojet}{\code{NNLOjet}}
\newcommand{\oneloop}{\code{OneL0op}}
\newcommand{\collier}{\code{Collier}}
\newcommand{\lhapdf}{\code{LHAPDF}}
\newcommand{\nnpdf}{\code{NNPDF3.1}}
\newcommand{\rivet}{\code{Rivet}}
\newcommand{\otter}{\code{Otter}}
\newcommand{\pentagonfunctions}{\code{PentagonFunctions++}}
\newcommand{\finiteflow}{\code{FiniteFlow}}
\newcommand{\qd}{\code{QD}}
\newcommand{\mathematica}{\code{Mathematica}}
\newcommand{\form}{\code{FORM}}
\newcommand{\qgraf}{\code{QGRAF}}
\newcommand{\spinney}{\code{Spinney}}
\newcommand{\fastjet}{\code{FastJet}}

\newcommand{\incite}[1]{Ref.~\cite{#1}}
\newcommand{\incites}[1]{Refs.~\cite{#1}}

\newcommand{\fp}{\texttt{f64}\xspace}
\newcommand{\fpp}{\texttt{f128}\xspace}
\newcommand{\fppp}{\texttt{f256}\xspace}

\newcommand{\sAr}[1]{| #1 \rangle}
\newcommand{\sAl}[1]{\langle #1 |}
\newcommand{\sBr}[1]{| #1 ]}
\newcommand{\sBl}[1]{[ #1 |}

\newcommand{\spAa}[1]{\langle#1\rangle}
\newcommand{\spBa}[1]{[#1]}

\newcommand{\spA}[2]{\spAa{#1 \, #2}}
\newcommand{\spB}[2]{\spBa{#1 \, #2}}
\newcommand{\spAB}[3]{\langle #1 \, #2 \, #3 ]}
\newcommand{\spBA}[3]{[ #1 \, #2 \, #3 \rangle}

\newcommand{\lra}{\xrightarrow{\hspace{2em}}}

\newcommand{\disc}[0]{\text{Disc}}
\newcommand{\discb}[1]{\disc\brf{#1}}
\newcommand{\discbb}[2]{\disc_{#1}\brf{#2}}
\newcommand{\li}[2]{\text{Li}_{#1}\brf{#2}}

\newcommand{\comment}[1]{\texttt{\textcolor{red}{#1}}}

\newcommand{\eps}{\epsilon}
\newcommand{\ord}{\mathcal{O}}

\newcommand{\mom}[2]{{#1}_{[#2]}}

\newcommand{\anti}[1]{\overline{#1}}

% \newcommand{\bigint}{\mathlarger{\mathlarger{\int}}}
% \newcommand{\bigsum}{\mathlarger{\mathlarger{\sum}}}
\newcommand{\bigint}{\mathlarger{\int}}
\newcommand{\bigsum}{\mathlarger{\sum}}

\usepackage[hidelinks, pdfusetitle]{hyperref} % hidelinks removes the boxes around links when viewed on screen, pdfusetitle option includes the thesis title and author in the PDF metadata, pagebackref includes links with pages numbers in bibliography to citations in body

% \usepackage[hidelinks, pdfusetitle, pagebackref]{hyperref}
% \renewcommand*{\backref}[1]{\{#1\}}

\usepackage[capitalise]{cleveref}
\makeatletter
\newcommand{\crefext}[2]{\csname cref@#1@format\endcsname{#2}{}{}}
\newcommand{\Crefext}[2]{\csname Cref@#1@format\endcsname{#2}{}{}}
\newcommand{\crefextp}[2]{\csname cref@#1@name@plural\endcsname{~#2}{}{}}
\newcommand{\Crefextp}[2]{\csname Cref@#1@name@plural\endcsname{~#2}{}{}}
\makeatother
