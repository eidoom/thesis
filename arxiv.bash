#!/usr/bin/env bash

perl -pi -e "s|usepackage{minted}|usepackage[finalizecache=true,cachedir=minted-cache]{minted}|g" preamble.tex

./build.sh

perl -pi -e "s|finalizecache|frozencache|g" preamble.tex

tar caf submission.tar.gz img/ minted-cache/ preamble.tex thesis.tex thesis.bbl ip3thesis.cls JHEP.bst code/c2_hel_amp_2.cpp code/s1_hel_amp_2.cpp

perl -pi -e "s|\[frozencache=true,cachedir=minted-cache\]||g" preamble.tex
