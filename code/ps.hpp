#pragma once

#include <cassert>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <numeric>
#include <vector>

#include "ngluon2/Mom.h"

// for QD compatibility
using std::abs;
using std::cos;
using std::pow;
using std::sin;
using std::sqrt;

namespace ps {

template <typename T>
bool test(const std::vector<MOM<T>>& moms, double zero);

template <typename T>
std::vector<MOM<T>>
five_point(const T y1, const T y2, const T theta = M_PI / 3, const T alpha = M_PI / 5, const T sqrtS = 1.);

}

template <typename T>
bool ps::test(const std::vector<MOM<T>>& moms, const double zero)
{
    for (std::size_t i { 0 }; i < moms.size(); ++i) {
        if (abs(moms[i].mass()) > zero) {
            std::cout << "m" << i << " = " << moms[i].mass() << '\n';
            return false;
        }
    }

    MOM<T> sum { std::accumulate(moms.cbegin(), moms.cend(), MOM<T>()) };

    for (int i { 0 }; i < 4; ++i) {
        if (sum.x(i) > zero) {
            std::cout << "p_sum^" << i << " = " << sum.x(i) << '\n';
            return false;
        }
    }

    return true;
}

template <typename T>
std::vector<MOM<T>>
ps::five_point(const T y1, const T y2, const T theta, const T alpha, const T sqrtS)
{
    assert(y1 >= 0.);
    assert(y2 >= 0.);
    assert(y1 <= 1.);
    assert(y2 <= 1.);
    assert(y1 + y2 >= 0.);
    assert(y1 + y2 <= 1.);
    assert(theta >= 0.);
    assert(theta <= M_PI);
    assert(alpha >= 0.);
    assert(alpha <= M_PI);

    const T x1 { 1. - y1 };
    const T x2 { 1. - y2 };
    assert(x1 >= 0.);
    assert(x2 >= 0.);
    assert(x1 <= 1.);
    assert(x2 <= 1.);
    assert(x1 + x2 >= 1.);
    assert(x1 + x2 <= 2.);

    const T cosBeta { 1. + 2. * (1. - x1 - x2) / (x1 * x2) };
    assert(-1. <= cosBeta);
    assert(cosBeta <= 1.);

    const T sinBeta { sqrt(1. - pow(cosBeta, 2.)) };
    assert(-1. <= sinBeta);
    assert(sinBeta <= 1.);

    const T cosTheta { cos(theta) },
        sinTheta { sin(theta) },
        cosAlpha { cos(alpha) },
        sinAlpha { sin(alpha) };

    std::vector<MOM<T>> momenta(5);

    momenta[0] = sqrtS / 2. * MOM<T>(-1., 0., 0., -1.);
    momenta[1] = sqrtS / 2. * MOM<T>(-1., 0., 0., 1.);
    momenta[2] = x1 * sqrtS / 2. * MOM<T>(1., sinTheta, 0., cosTheta);
    momenta[3] = x2 * sqrtS / 2. * MOM<T>(1., cosAlpha * cosTheta * sinBeta + cosBeta * sinTheta, sinAlpha * sinBeta, cosBeta * cosTheta - cosAlpha * sinBeta * sinTheta);
    momenta[4] = -std::accumulate(momenta.cbegin(), momenta.cend() - 1, MOM<T>());

    assert(ps::test(momenta));

    return momenta;
}
