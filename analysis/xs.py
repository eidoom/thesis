#!/usr/bin/env python3

import argparse, pathlib, operator, math
import numpy, matplotlib.pyplot, matplotlib.cm

from loader import load_cut_data, load_raw_data
from plot import PHI, init_plots


def get_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-r",
        "--refresh-cache",
        action="store_true",
        help="Recalculate all cached values",
    )
    parser.add_argument(
        "-c",
        "--cuts",
        action="store_true",
        help="Apply cuts to data",
    )
    parser.add_argument(
        "-n",
        "--check-sqrt-n",
        action="store_true",
        help="Check the scaling behaviour of the MC error",
    )
    parser.add_argument(
        "-t",
        "--plot-total-xs",
        action="store_true",
        help="Plot the total cross section as a function of the number of points sampled",
    )
    parser.add_argument(
        "-v",
        "--check-covariance-matrix",
        action="store_true",
        help="Check that the total cross section error can be reconstructed from the covariance matrix of the bins",
    )
    parser.add_argument(
        "-p",
        "--plot-histogram",
        action="store_true",
        help="Plot the histogram of the differential cross section",
    )

    args = parser.parse_args()

    if (
        args.refresh_cache
        and not input("Are you sure you want to refresh the cache? [y] ") == "y"
    ):
        exit()

    return args


def histogram(bin_edges, y, errs):
    filename = "pt_j1.pdf"
    print(f"Plotting {filename}")

    height_ratios = (1, 1 / PHI)
    nrows = len(height_ratios)
    aspect_ratio = 1
    w = 4.8

    colours = matplotlib.cm.tab10(range(nrows))

    x = (bin_edges[:-1] + bin_edges[1:]) / 2  # bin_centres

    fig, axs = matplotlib.pyplot.subplots(
        figsize=(w, w / aspect_ratio),
        nrows=nrows,
        sharex=True,
        height_ratios=height_ratios,
    )
    fig.subplots_adjust(
        wspace=0,
        hspace=0,
    )

    axs[0].step(
        x,
        y,
        where="mid",
        color=colours[0],
        linewidth=1.5,
    )
    axs[0].errorbar(
        x,
        y,
        linestyle="",
        yerr=errs,
        color=colours[1],
        elinewidth=1.5,
    )
    axs[0].set_yscale("log")
    axs[0].set_ylabel(r"$\mathrm{d} \sigma / \mathrm{d} p_{T,j_1}$")

    axs[1].bar(
        x,
        errs / ys,
        color=colours[1],
        width=1.5,
    )
    axs[1].set_ylabel("Relative error")

    axs[-1].set_xlabel(r"$p_{T,j_1}$ [GeV]")

    fig.savefig(filename, bbox_inches="tight")


def result_and_error(events, n):
    mean = numpy.sum(events) / n
    return mean, (
        math.sqrt(numpy.sum((events - mean) ** 2) / (n - 1) / n) if n > 1 else 0
    )


def total_xs(events):
    # this answer is = (total cross section) / (integration volume)
    return result_and_error(events, len(events))


def bin_points(bin_edges, pt_j1, events):
    return numpy.array(
        [
            numpy.where(numpy.logical_and(e0 <= pt_j1, pt_j1 < e1), events, 0)
            for e0, e1 in zip(bin_edges[:-1], bin_edges[1:])
        ]
    )


if __name__ == "__main__":
    args = get_args()

    if args.plot_histogram or args.plot_total_xs:
        init_plots()

    pt_j1, events = (
        load_cut_data(args.refresh_cache)
        if args.cuts
        else load_raw_data(args.refresh_cache)
    )

    n = len(events)

    if args.check_sqrt_n:
        for nn in map(int, numpy.linspace(1e5, n, 10)):
            txs, te = total_xs(events[:nn])
            print(f"{nn}: {txs:.1e} +- {te:.1e} ({100 * te / txs:.1e}%)")

    if args.plot_total_xs:
        left = 1e4
        right = n
        ns = numpy.linspace(left, right, 100, dtype=int)
        integrals, errors = numpy.transpose(
            [total_xs(numpy.random.choice(events, size=nn, replace=False)) for nn in ns]
        )

        colours = matplotlib.cm.tab10(range(2))

        fig, ax = matplotlib.pyplot.subplots()
        ax.errorbar(
            ns,
            integrals,
            yerr=errors,
            color=colours[0],
            ecolor=colours[1],
        )
        ax.hlines(integrals[-1], left, right, color="black")
        ax.set_xlabel("Number of points sampled")
        ax.set_ylabel("Total cross section")
        fig.savefig("total_cross_section.pdf", bbox_inches="tight")

        fig, ax = matplotlib.pyplot.subplots()
        ax.scatter(ns, errors)
        ax.plot(ns, 3.8e-6 / numpy.sqrt(ns), color="black")
        ax.set_xlabel("Number of points sampled")
        ax.set_ylabel("Absolute error on total cross section")
        fig.savefig("total_cross_section_error.pdf", bbox_inches="tight")

    print("Number of events passing vetoes:", n)

    xs, err = total_xs(events)
    print(
        f"Total cross section: {xs:.1e} +- {err:.1e} (relative: {100 * err / xs:.1e}%)"
    )

    nbins = 50
    bin_edges = numpy.histogram_bin_edges(
        a=pt_j1,
        bins=nbins,
    )
    bin_width = bin_edges[1] - bin_edges[0]

    yss = bin_points(bin_edges, pt_j1, events) / bin_width

    ys, errs = numpy.array([result_and_error(ys, n) for ys in yss]).T

    cov = numpy.cov(yss)
    errs_cov = numpy.sqrt(cov.diagonal()) / math.sqrt(n)
    assert numpy.allclose(errs_cov, errs, rtol=1e-16, atol=1e-16)

    diag_reconstruct = sum(numpy.square(errs)) * bin_width**2
    print(
        f"If errors add in quadrature, variance ratio should be >= unity: {err**2 / diag_reconstruct:.4e}"
    )

    print(
        f"If area under histogram is total XS, ratio should be unity: {xs / sum(ys * bin_width):.2e}"
    )

    if args.check_covariance_matrix:
        A = numpy.ones(nbins)
        full_reconstructed = ((A @ cov) @ A.T) / n * bin_width**2
        print(f"covariance ratio: {err**2 / full_reconstructed:.4e}")

        n1 = 30
        edges1 = numpy.histogram_bin_edges(
            a=pt_j1,
            bins=n1,
        )
        yss1 = bin_points(edges1, pt_j1, events)
        ys1 = numpy.sum(yss1, axis=1) / n
        cov1 = numpy.cov(yss1)

        n2 = n1 // 2
        edges2 = numpy.histogram_bin_edges(
            a=pt_j1,
            bins=n2,
        )
        yss2 = bin_points(edges2, pt_j1, events)
        ys2 = numpy.sum(yss2, axis=1) / n
        cov2 = numpy.cov(yss2)

        AA = numpy.zeros(shape=(n2, n1))
        for i in range(n2):
            for j in range(2):
                AA[i, 2 * i + j] = 1

        cov2a = (AA @ cov1) @ AA.T

        # ys2a = numpy.array(
        #     [ys1[i : i + 1].sum() for i in range(0, n1 - 1, 2)]
        # )

        # print(edges1[2], edges2[1])

        # print(ys2[:4])
        # print(ys2a[:4])

        # print(cov2.diagonal()[:4])
        # print(cov2a.diagonal()[:4])

        # print(numpy.allclose(ys2, ys2a))
        print(
            "Rebinned covariance matrix equal to original:", numpy.allclose(cov2, cov2a)
        )

    if args.plot_histogram:
        histogram(bin_edges, ys, errs)
