import pathlib

import numpy

from plot import load, load_pdfs


def safe_indices(points, beam_energy, refresh_cache=False):
    cache = f"data/safe_indices.npy"

    if refresh_cache or not pathlib.Path(cache).exists():
        indices = numpy.array(
            [
                i
                for i, point in enumerate(points)
                if all(point[i] < beam_energy for i in (0, 4))
            ]
        )
        numpy.save(cache, indices)
        return indices

    return numpy.load(cache)


def load_raw_data(refresh_cache):
    cachex = "data/pt_j1.npy"
    cachey = "data/weights.npy"

    if (
        refresh_cache
        or not pathlib.Path(cachex).exists()
        or not pathlib.Path(cachey).exists()
    ):
        kin_name = "data/mom_6pt_1M"
        beam_energy = 500  # GeV

        amps = load("data/amp_6pt_1M", refresh_cache)
        moms = load(kin_name, refresh_cache)

        # veto phase space points with energy on an incoming particle greater than the beam energy
        safe = safe_indices(moms, beam_energy, refresh_cache)
        moms = moms[safe]
        amps = amps[safe]

        pdfs = load_pdfs(kin_name, beam_energy, moms, refresh_cache)

        # veto phase space points with a negative PDF value
        positive = numpy.array([i for i, pdf in enumerate(pdfs) if pdf >= 0])
        pdfs = pdfs[positive]
        moms = moms[positive]
        amps = amps[positive]

        events = numpy.multiply(amps, pdfs)  # Uniform PS weights
        pt_j1 = numpy.hypot(moms[:, 17], moms[:, 18])

        numpy.save(cachex, pt_j1)
        numpy.save(cachey, events)

        return pt_j1, events

    return numpy.load(cachex), numpy.load(cachey)


# def pt_j1(psp):
#     j1 = psp[4 * 2 : 4 * 3]
#     return math.sqrt(j1[1] ** 2 + j1[2] ** 2)


def theta(psp, i):
    return math.acos(psp[i * 4 + 3] / psp[i * 4])


def dtheta(psp, i, j):
    return math.acos(
        numpy.dot(psp[i * 4 + 1 : i * 4 + 4], psp[j * 4 + 1 : j * 4 + 4])
        / (psp[i * 4] * psp[j * 4])
    )


def eta(psp, i):
    return abs(-math.log(math.tan(theta(psp, i) / 2)))


def deta(psp, i, j):
    return abs(-math.log(math.tan(dtheta(psp, i, j) / 2)))


# def phi(psp, i):
#     return math.atan(psp[i * 4 + 2] / psp[i * 4 + 1])


# def phi_alt(psp, i):
#     pt = math.sqrt(psp[i * 4 + 1] ** 2 + psp[i * 4 + 2] ** 2)
#     phi_ = math.acos(psp[i * 4 + 1] / pt)
#     if phi_ > math.pi / 2:
#         return phi_ - math.pi
#     return phi_


def dphi(psp, i, j):
    pti = math.sqrt(psp[i * 4 + 1] ** 2 + psp[i * 4 + 2] ** 2)
    ptj = math.sqrt(psp[j * 4 + 1] ** 2 + psp[j * 4 + 2] ** 2)
    dot = psp[i * 4 + 1] * psp[j * 4 + 1] + psp[i * 4 + 2] * psp[j * 4 + 2]
    phi_ = math.acos(dot / (pti * ptj))
    if phi_ > math.pi / 2:
        return phi_ - math.pi
    return phi_


def dr(psp, i, j):
    return numpy.hypot(deta(psp, i, j), dphi(psp, i, j))


def load_cut_data(refresh_cache):
    cache = "data/events_cut.npy"
    cachex = "data/pt_j1_cut.npy"

    if (
        refresh_cache
        or not pathlib.Path(cache).exists()
        or not pathlib.Path(cachex).exists()
    ):
        pt_j1, events = load_raw_data(refresh_cache)

        # g g y y g g

        pt_j1_pass = numpy.array([i for i, pt in enumerate(pt_j1) if pt > 20])
        print("pt_j1", len(pt_j1_pass))

        pt_j2 = numpy.hypot(moms[:, 21], moms[:, 22])
        pt_j2_pass = numpy.array([i for i, pt in enumerate(pt_j2) if pt > 20])
        print("pt_j2", len(pt_j2_pass))

        eta_j1 = numpy.array([eta(psp, 4) for psp in moms])
        eta_j1_pass = numpy.array([i for i, e in enumerate(eta_j1) if e < 5])
        print("eta_j1", len(eta_j1_pass))

        eta_j2 = numpy.array([eta(psp, 5) for psp in moms])
        eta_j2_pass = numpy.array([i for i, e in enumerate(eta_j2) if e < 5])
        print("eta_j2", len(eta_j2_pass))

        eta_y1 = numpy.array([eta(psp, 2) for psp in moms])
        eta_y1_pass = numpy.array([i for i, e in enumerate(eta_y1) if e < 2.37])
        print("eta_y1", len(eta_y1_pass))

        eta_y2 = numpy.array([eta(psp, 3) for psp in moms])
        eta_y2_pass = numpy.array([i for i, e in enumerate(eta_y2) if e < 2.37])
        print("eta_y2", len(eta_y2_pass))

        pt_y1 = numpy.hypot(moms[:, 9], moms[:, 10])
        pt_y2 = numpy.hypot(moms[:, 13], moms[:, 14])
        pt_y_pass = []
        for i, (y1, y2) in enumerate(zip(pt_y1, pt_y2)):
            if y1 > y2:
                if y1 > 40 and y2 > 30:
                    pt_y_pass.append(i)
            else:
                if y1 > 30 and y2 > 40:
                    pt_y_pass.append(i)
        print("pt_y", len(pt_y_pass))

        r_yy = numpy.array([dr(psp, 2, 3) for psp in moms])
        r_yy_pass = numpy.array([i for i, r in enumerate(r_yy) if r > 0.4])
        print("r_yy", len(r_yy_pass))

        r_y1j1 = numpy.array([dr(psp, 2, 4) for psp in moms])
        r_y1j1_pass = numpy.array([i for i, r in enumerate(r_y1j1) if r > 0.4])
        print("r_y1j1", len(r_y1j1_pass))

        r_y1j2 = numpy.array([dr(psp, 2, 5) for psp in moms])
        r_y1j2_pass = numpy.array([i for i, r in enumerate(r_y1j2) if r > 0.4])
        print("r_y1j2", len(r_y1j2_pass))

        r_y2j1 = numpy.array([dr(psp, 3, 4) for psp in moms])
        r_y2j1_pass = numpy.array([i for i, r in enumerate(r_y2j1) if r > 0.4])
        print("r_y2j1", len(r_y2j1_pass))

        r_y2j2 = numpy.array([dr(psp, 3, 5) for psp in moms])
        r_y2j2_pass = numpy.array([i for i, r in enumerate(r_y2j2) if r > 0.4])
        print("r_y2j2", len(r_y2j2_pass))

        passes = set.intersection(
            *map(
                set,
                (
                    pt_j1_pass,
                    pt_j2_pass,
                    eta_j1_pass,
                    eta_j2_pass,
                    eta_y1_pass,
                    eta_y2_pass,
                    pt_y_pass,
                    r_yy_pass,
                    r_y1j1_pass,
                    r_y1j2_pass,
                    r_y2j1_pass,
                    r_y2j2_pass,
                ),
            )
        )
        passes = numpy.array(sorted(passes))
        print("passes", len(passes))

        data = events[passes]
        x = pt_j1[passes]

        numpy.save(cache, data)
        numpy.save(cachex, x)

        return x, data

    return numpy.load(cachex), numpy.load(cache)


if __name__ == "__main__":
    pass
