#include <array>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include <qd/qd_real.h>

#include "analytic/0q4g-analytic.h"
#include "analytic/0q5g-analytic.h"
#include "chsums/0q6g.h"
#include "ir/soft_gg2g-analytic.h"
#include "ir/split_g2gg-analytic.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"
#include "tools/PhaseSpace.h"

#include "ps.hpp"

// for QD compatibility
using std::abs;
using std::pow;
using std::sqrt;

constexpr double ZERO { 1e-8 };

std::vector<std::vector<MOM<double>>>
read_moms(const std::string& filename)
{
  const int d { 4 };

  // std::cout << "Reading momenta..." << '\n';

  std::ifstream infile(filename);
  std::string line, num;
  std::vector<std::vector<MOM<double>>> points;

  while (std::getline(infile, line)) {
    std::vector<MOM<double>> point;
    MOM<double> mom;

    std::istringstream iss(line);

    int i { 0 };
    while (std::getline(iss, num, ' ')) {
      switch (i++) {
      case 0:
        mom.x0 = std::stod(num);
        break;
      case 1:
        mom.x1 = std::stod(num);
        break;
      case 2:
        mom.x2 = std::stod(num);
        break;
      case 3:
        mom.x3 = std::stod(num);
        point.push_back(mom);
        i = 0;
      }
    }

    assert(ps::test(point, ZERO));

    points.push_back(point);
  }

  return points;
}

template <typename T, template <typename> class AMP_FULL, template <typename> class AMP_RED>
void soft(const int j, const std::vector<std::vector<MOM<double>>>& psps, const std::vector<std::vector<std::vector<std::vector<MOM<double>>>>>& red_psps, const std::string& name)
{
  const double mur { 91.188 }, sqrtS { 1. };
  // leg j is soft (zero index)
  const std::size_t mf { psps[0].size() }, mr { mf - 1 };
  const int Nc { 3 }, Nf { 5 };
  const std::vector<double> scales2 { { 0. } }; // no associated scales

  AMP_FULL<T> amp_full;
  amp_full.setNf(Nf);
  amp_full.setNc(Nc);
  amp_full.setMuR2(std::pow(mur, 2));

  AMP_RED<T> amp_red;
  amp_red.setNf(Nf);
  amp_red.setNc(Nc);
  amp_red.setMuR2(std::pow(mur, 2));

  Softgg2g_a<T> amp_soft;
  amp_soft.setNf(Nf);
  amp_soft.setNc(Nc);
  amp_soft.setMuR2(std::pow(mur, 2));

  std::cout << std::scientific << std::setprecision(1);

  std::cout << "    Writing to " << name << '\n';
  std::ofstream o(name, std::ios::out);
  o << std::scientific << std::setprecision(16);

  for (std::size_t a { 0 }; a < psps.size(); ++a) {
    //std::vector<MOM<T>> mom_full(mf);
    std::vector<MOM<T>> mom_full { psps[a] };
    // refineM(psps[a], mom_full, scales2);

    // \hat E
    const T eh { mom_full[j].x(0) / sqrt(2. * dot(mom_full[0], mom_full[1])) };

    amp_full.setMomenta(mom_full);

    // compute full matrix element
    const T val_amp { amp_full.born() };

    // perform reduced colour sum
    // indices i,j,k are in the full phase space
    T lim {};
    std::size_t ref_index {};
    for (std::size_t i { 0 }; i < mf; ++i) {
      if (i != j) {
        for (std::size_t k { i + 1 }; k < mf; ++k) {
          if (k != j) {
	    //std::cout << "i="<<i<<" j="<<j<<" k="<<k<<"\n";
            // reduced phase space (with soft leg as a zero momentum)
            std::vector<MOM<T>> mom_pseudo { red_psps[i][k][a] };
	    //std::cout << "E\n";

            assert(ps::test(mom_pseudo, ZERO));

            // reduced phase space (omit soft leg)
            std::vector<MOM<T>> mom_red(mr);
            std::copy_n(mom_pseudo.cbegin(), j, mom_red.begin());
            std::copy(mom_pseudo.cbegin() + j + 1, mom_pseudo.cend(), mom_red.begin() + j);

            // refineM(mom_red, mom_red, scales2);

            amp_red.setMomenta(mom_red);

            // ensure reference momentum is not the same as another momentum in use
            // to avoid division by zero from brackets in the denominator of the form < i i >
            while (ref_index == i || ref_index == j || ref_index == k) {
              ref_index = (ref_index + 1) % mf;
            }

            // set momenta for eikonal amplitude
            // middle momentum is the soft leg
            // the second argument is the reference momentum
            amp_soft.setMomenta({ mom_pseudo[i], mom_full[j], mom_pseudo[k] }, mom_full[ref_index]);

            // compute eikonal and colour-correlated reduced matrix elements
            const T val_soft { amp_soft.born() },
                cc_val { amp_red.born_ccij(i < j ? i : (i - 1) % mr, k < j ? k : (k - 1) % mr) };

            lim += cc_val * val_soft;
          }
        }
      }
    }

    // we summed over upper triangle of colour correlation matrix
    // diagonals receive no contribution because eikonal kinematic factor is zero due to antisymmetry of brackets
    // matrix is symmetric, so add lower triangle by doubling result
    lim *= 2.;

    // helicity sum factor
    lim /= 4.;

    o << val_amp << ' ' << lim << '\n';

    // relative difference
    const T rd { 1. - lim / val_amp };

    std::cout << "    point  " << std::setw(3) << a + 1 << " / " << psps.size() << ' ' << eh << ' ' << rd << '\n';
  }
}

template <template <typename> class AMP_FULL, template <typename> class AMP_RED>
void iter_prec(const int legs, const int num)
{
  std::cout << "Running for " << legs << " legs\n";
  const int jj { 3 }; // soft leg

  for (int i { 0 }; i < num; ++i) {
    const std::string filename { "data/soft.ps." + std::to_string(legs) + "pt." + std::to_string(i) + ".csv" };
    std::cout << "  rseed  " << std::setw(3) << i + 1 << "/" << num << " " << filename << "\n";
    const std::vector<std::vector<MOM<double>>> moms { read_moms(filename) };

    std::vector<std::vector<std::vector<std::vector<MOM<double>>>>> red_moms(legs, std::vector<std::vector<std::vector<MOM<double>>>>(legs));
    for (int ii { 0 }; ii < legs; ++ii) {
      for (int kk { ii + 1 }; kk < legs; ++kk) {
	if(ii==jj||kk==jj) continue;
	const std::string red_filename{"data/soft.ps." + std::to_string(legs) + "pt." + std::to_string(i) + ".reduced.i=" + std::to_string(ii) + ".j=" + std::to_string(jj) + ".k=" + std::to_string(kk) + ".csv"};
	//std::cout<<red_filename<<'\n';
        red_moms[ii][kk] = read_moms(red_filename);
      }
    }

    soft<double, AMP_FULL, AMP_RED>(jj, moms, red_moms, "data/soft." + std::to_string(legs) + "pt.f64." + std::to_string(i) + ".csv");
    //soft<dd_real, AMP_FULL, AMP_RED>(jj, moms, red_moms, "data/soft." + std::to_string(legs) + "pt.f128." + std::to_string(i) + ".csv");
    //soft<qd_real, AMP_FULL, AMP_RED>(jj, moms, red_moms, "data/soft." + std::to_string(legs) + "pt.f256." + std::to_string(i) + ".csv");
  }
}

int main(int argc, char* argv[])
{
  if (argc != 2) {
    std::cerr << "Wrong number of command lines arguments!" << '\n'
              << "Use as: ./run_soft <number of repeated paths in phase space>" << '\n';
    std::exit(EXIT_FAILURE);
  }

  const int num { std::atoi(argv[1]) };

  iter_prec<Amp0q5g_a, Amp0q4g_a>(5, num);

  iter_prec<Amp0q6g, Amp0q5g_a>(6, num);
}
