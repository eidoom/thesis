# Minor Corrections

## Chapter 1

* [x] 1.2.3: rework the explanation of large separation of scales, resummation is perturbative!
* [x] 1.2.3: infrared safety to be explained better
* [x] 1.4.1: rephrase the sentence \'partial amplitudes \[..\] have simpler kinematics than the full amplitude\'
* [x] 1.5 2x2 matrix -> nxn
* [x] P 27 LI -> unrelated , spinor map unique
* [x] Why Schouten identity is include in twistor variables?
* [x] Explain discontinuity
* [x] 1.6 Explain how a cut work operationally better!
* [x] 1.6.31 why is this working for all v_i?
* [x] 1.7 integers have no precision loss (for operation modulo n!)
* [x] 1.8.3 ... first complete 2-\>2 process to be calculated (at the order!)
* [x] 1.8 Cite Cacciari <https://arxiv.org/abs/1105.5152.pdf> , and others <https://arxiv.org/abs/2006.16293.pdf> in efforts to estimate theory uncertainty
* [x] 1.9 missing word in line 6/7 of paragraph (probably necessary)

## Chapter 2

* [x] 2.1.1 clarify "pseudojet"
* [x] 2.1.2 KLN for infrared safe obs
* [x] 2.2.1 use consistent notation n/n+1 vs n-1/n
* [x] 2.2.3 is there a reference for 2.2.27
* [x] 2.4 showing scaling would be useful

##  Chapter 3

* [x] After 3.3.3, "double counting" is it "multiple" counting rather than just twice?
* [x] Figure 3.5 unstacked the histogram
* [x] Pdf weighted 3.5
* [ ] Think about what the issue is with the discrepancy between the precision of the distributions and the large error in the total cross section.
    * [Non-normalised cross section distributions (NJet 3M/NN 1.5M)](https://ippp.dur.ac.uk/~jbullock/files/research/parallel_fixed/integration_grid/4g2A-100k-njet-nn-unit-grid-delta-0001-3M/rivet-plots/diphoton/index.html)

## Chapter 4

* [x] Caption of 4.6, definition of cumulative wrong, also in the text
