#include <cassert>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
#include <vector>

#include <qd/dd_real.h>

#include "analytic/0q4g-analytic.h"
#include "analytic/0q5g-analytic.h"
#include "chsums/0q6g.h"
#include "ir/soft_gg2g-analytic.h"
#include "ir/split_g2gg-analytic.h"
#include "ngluon2/EpsTriplet.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"
#include "tools/PhaseSpace.h"

#include "cps.hpp"

// QD compatibility
using std::abs;
using std::pow;

template <typename T>
T relative_difference(T value, const std::complex<T>& limit)
{
  return 1. - limit.real() / value;
}

template <typename T>
T relative_difference(const std::complex<T>& value, const std::complex<T>& limit)
{
  return relative_difference(value.real(), limit);
}

template <typename T, template <typename> class AMP_FULL, template <typename> class AMP_RED>
void collinear(const int mf, const int rseed, const std::string& name, const bool one_loop)
{
  const double mur { 91.188 }, sqrtS { 1. };
  const T z { 0.5 }, base { 1.1 }, step { 1.8 };
  const int Nc { 3 }, Nf { 5 }, start { 26 }, num { 100 }, mr { mf - 1 };
  const std::vector<double> scales2 { { 0. } }; // no associated scales

  PhaseSpace<T> ps4(mr, rseed, sqrtS);
  std::vector<MOM<T>> mom_base { ps4.getPSpoint() };
  refineM(mom_base, mom_base, scales2);

  AMP_FULL<T> amp_full;
  amp_full.setNf(Nf);
  amp_full.setNc(Nc);
  amp_full.setMuR2(std::pow(mur, 2));

  AMP_RED<T> amp_red;
  amp_red.setNf(Nf);
  amp_red.setNc(Nc);
  amp_red.setMuR2(std::pow(mur, 2));

  Splitg2gg_a<T> amp_spl;
  amp_spl.setNf(Nf);
  amp_spl.setNc(Nc);
  amp_spl.setMuR2(std::pow(mur, 2));

  std::cout << std::scientific << std::setprecision(1);

  std::ofstream o(name, std::ios::out);
  o << std::scientific << std::setprecision(16);

  for (int a { 0 }; a < num; ++a) {
    // degree of collinearity
    const T d { pow(base, -(start + a * step)) };

    // 2 || 3 non-soft collinear massless mf-particle phase space (zero indexed)
    std::vector<MOM<T>> mom_full { cps::collinear(mom_base, z, d, 2) };
    refineM(mom_full, mom_full, scales2);

    // \hat s_{23}
    const T sh23 { dot(mom_full[2], mom_full[3]) / dot(mom_full[0], mom_full[1]) };

    // Catani-Seymour momentum mapping for 2 || 3 (0 indexing)
    const MOM<T> p { mom_full[2] + mom_full[3] };
    const T x { dot(mom_full[2], mom_full[3]) / dot(p, mom_full[4]) };

    // reduced phase space
    std::vector<MOM<T>> mom_red(mr);
    std::copy_n(mom_full.cbegin(), 2, mom_red.begin());
    mom_red[2] = p - x * mom_full[4];
    mom_red[3] = (1. + x) * mom_full[4];
    std::copy(mom_full.cbegin() + 5, mom_full.cend(), mom_red.begin() + 4);

    refineM(mom_red, mom_red, scales2);

    // momenta for splitting amplitude
    const std::vector<MOM<T>> mom_spl { { -mom_red[2], mom_full[2], mom_full[3] } };

    amp_full.setMomenta(mom_full);
    amp_red.setMomenta(mom_red);
    amp_spl.setMomenta(mom_spl, mom_red[3]);

    // initialise (flattened) 2x2 spin matrices
    // order: ++ +- -+ --
    std::vector<std::complex<T>> spn_mat_red_0x0(4), spn_mat_spl_0x0(4);

    // compute spin matrices
    amp_red.born_spnmatrix(2, spn_mat_red_0x0);
    amp_spl.born_spnmatrix(0, spn_mat_spl_0x0);

    // while the final result will be real, the intermediate steps may be complex
    std::complex<T> lim_tree_diag {};
    std::complex<T> lim_tree_corr {};

    for (int i : { 0, 3 }) {
      lim_tree_diag += spn_mat_red_0x0[i] * spn_mat_spl_0x0[3 - i];
    }

    for (int i : { 1, 2 }) {
      lim_tree_corr += spn_mat_red_0x0[i] * spn_mat_spl_0x0[3 - i];
    }

    // compute full matrix element
    const T val_tree { amp_full.born() };

    o << sh23 << ' ' << val_tree << ' ' << lim_tree_diag.real() << ' ' << lim_tree_corr.real();

    // relative difference
    const T rdt { relative_difference(val_tree, lim_tree_diag + lim_tree_corr) };

    std::cout << std::setw(3) << a + 1 << " / " << num << ' ' << sh23 << ": " << rdt;

    if (one_loop) {
      std::vector<Eps3<T>> spn_mat_red_1x0(4), spn_mat_spl_1x0(4);

      amp_red.virt_spnmatrix(2, spn_mat_red_1x0);
      amp_spl.virt_spnmatrix(0, spn_mat_spl_1x0);

      Eps3<T> lim_loop_diag {};
      Eps3<T> lim_loop_corr {};

      for (int i : { 0, 3 }) {
        lim_loop_diag += spn_mat_red_1x0[i] * spn_mat_spl_0x0[3 - i];
        lim_loop_diag += spn_mat_red_0x0[i] * spn_mat_spl_1x0[3 - i];
      }

      for (int i : { 1, 2 }) {
        lim_loop_corr += spn_mat_red_1x0[i] * spn_mat_spl_0x0[3 - i];
        lim_loop_corr += spn_mat_red_0x0[i] * spn_mat_spl_1x0[3 - i];
      }

      // loop factor
      const T loop_factor = T(9.) / T(2.);
      lim_loop_diag *= loop_factor;
      lim_loop_corr *= loop_factor;

      const Eps3<T> val_loop { amp_full.virt() };

      o
          << ' ' << val_loop.get2().real() << ' ' << lim_loop_diag.get2().real() << ' ' << lim_loop_corr.get2().real()
          << ' ' << val_loop.get1().real() << ' ' << lim_loop_diag.get1().real() << ' ' << lim_loop_corr.get1().real()
          << ' ' << val_loop.get0().real() << ' ' << lim_loop_diag.get0().real() << ' ' << lim_loop_corr.get0().real();

      const T rd2 { relative_difference(val_loop.get2(), (lim_loop_diag + lim_loop_corr).get2()) };
      const T rd1 { relative_difference(val_loop.get1(), (lim_loop_diag + lim_loop_corr).get1()) };
      const T rd0 { relative_difference(val_loop.get0(), (lim_loop_diag + lim_loop_corr).get0()) };

      std::cout << ", " << rd2 << "/e^2 + " << rd1 << "/e + " << rd0;
    }

    o << '\n';
    std::cout << '\n';
  }

  if (one_loop) {
    std::cout << "Now it will error because memory has been managed poorly somewhere in virt_spnmatrix - but we've already got what we wanted." << '\n';
  }
}

// // need e^1, e^2 coefficients for this!
// template <typename T>
// void collinear5_1Lsq(const int rseed, const std::string& name, const bool correlations = true)
// {
//   const double mur { 91.188 }, sqrtS { 1. };
//   const T z { 0.5 }, base { 1.1 }, step { 1.8 };
//   const int Nc { 3 }, Nf { 5 }, start { 26 }, num { 100 };
//   const std::vector<double> scales2 { { 0. } }; // no associated scales
//
//   PhaseSpace<T> ps4(4, rseed, sqrtS);
//   std::vector<MOM<T>> mom_base { ps4.getPSpoint() };
//   refineM(mom_base, mom_base, scales2);
//
//   Amp0q5g_a<T> amp_full;
//   amp_full.setNf(Nf);
//   amp_full.setNc(Nc);
//   amp_full.setMuR2(std::pow(mur, 2));
//
//   Amp0q4g_a<T> amp_red;
//   amp_red.setNf(Nf);
//   amp_red.setNc(Nc);
//   amp_red.setMuR2(std::pow(mur, 2));
//
//   Splitg2gg_a<T> amp_spl;
//   amp_spl.setNf(Nf);
//   amp_spl.setNc(Nc);
//   amp_spl.setMuR2(std::pow(mur, 2));
//
//   std::cout << std::scientific << std::setprecision(1);
//
//   std::ofstream o(name, std::ios::out);
//   o << std::scientific << std::setprecision(16);
//
//   for (int a { 0 }; a < num; ++a) {
//     // degree of collinearity
//     const T d { pow(base, -(start + a * step)) };
//
//     // 2 || 3 non-soft collinear massless 5-particle phase space (zero indexed)
//     std::vector<MOM<T>> mom_full { cps::collinear(mom_base, z, d, 2) };
//     refineM(mom_full, mom_full, scales2);
//
//     // \hat s_{23}
//     const T sh23 { dot(mom_full[2], mom_full[3]) / dot(mom_full[0], mom_full[1]) };
//
//     // Catani-Seymour momentum mapping for 2 || 3 (0 indexing)
//     const MOM<T> p { mom_full[2] + mom_full[3] };
//     const T x { dot(mom_full[2], mom_full[3]) / dot(p, mom_full[4]) };
//
//     // reduced phase space
//     std::vector<MOM<T>> mom_red(4);
//     std::copy_n(mom_full.cbegin(), 2, mom_red.begin());
//     mom_red[2] = p - x * mom_full[4];
//     mom_red[3] = (1. + x) * mom_full[4];
//
//     refineM(mom_red, mom_red, scales2);
//
//     // momenta for splitting amplitude
//     const std::vector<MOM<T>> mom_spl { { -mom_red[2], mom_full[2], mom_full[3] } };
//
//     amp_full.setMomenta(mom_full);
//     amp_red.setMomenta(mom_red);
//     amp_spl.setMomenta(mom_spl, mom_red[3]);
//
//     // initialise (flattened) 2x2 spin matrices
//     std::vector<std::complex<T>> spn_mat_red_0x0(4), spn_mat_spl_0x0(4);
//     std::vector<Eps3<T>> spn_mat_red_1x0(4), spn_mat_spl_1x0(4);
//     std::vector<Eps5<T>> spn_mat_red_1x1(4), spn_mat_spl_1x1(4);
//
//     // compute spin matrices
//     amp_red.born_spnmatrix(2, spn_mat_red_0x0);
//     amp_spl.born_spnmatrix(0, spn_mat_spl_0x0);
//     amp_red.virt_spnmatrix(2, spn_mat_red_1x0);
//     amp_spl.virt_spnmatrix(0, spn_mat_spl_1x0);
//     amp_red.virtsq_spnmatrix(2, spn_mat_red_1x1);
//     amp_spl.virtsq_spnmatrix(0, spn_mat_spl_1x1);
//
//     // while the final result will be real, the intermediate steps may be complex
//     std::complex<T> lim_tree {};
//     Eps5<T> lim_loop {};
//
//     // compute factorised matrix element including spin correlations
//     // order: ++ +- -+ --
//     for (int i { 0 }; i < 4; ++i) {
//       if (correlations || i == 0 || i == 3) {
//         lim_tree += spn_mat_red_0x0[i] * spn_mat_spl_0x0[3 - i];
//
//         lim_loop += spn_mat_red_0x0[i] * spn_mat_spl_1x1[3 - i];
//         lim_loop += spn_mat_red_1x1[i] * spn_mat_spl_0x0[3 - i];
//         lim_loop += 2 * spn_mat_red_1x0[i] * spn_mat_spl_1x0[3 - i];
//       }
//     }
//     lim_loop *= T(9.) / T(2.); // chjeck factor
//
//     // compute full matrix element
//     const T val_tree { amp_full.born() };
//     const Eps5<T> val_loop { amp_full.virtsq() };
//
//     // relative difference
//     const T rdt { relative_difference(val_tree, lim_tree) };
//     const T rd0 { relative_difference(val_loop.get0(), lim_loop.get0()) };
//     const T rd1 { relative_difference(val_loop.get1(), lim_loop.get1()) };
//     const T rd2 { relative_difference(val_loop.get2(), lim_loop.get2()) };
//
//     o << sh23 << ' ' << rdt << ' ' << rd2 << ' ' << rd1 << ' ' << rd0 << '\n';
//
//     std::cout << std::setw(3) << a + 1 << " / " << num << ' ' << sh23 << ": " << rdt << ", " << rd2 << "/e^2 + " << rd1 << "/e + " << rd0
//               << '\n';
//   }
//
//   std::cout << "Now it will error because memory has been managed poorly somewhere in the NJet IR library - but we've already got what we wanted." << '\n';
// }

int main(int argc, char* argv[])
{
  if (argc != 2) {
    std::cerr << "Wrong number of command lines arguments!" << '\n'
              << "Use as: ./calc_otf <rseed #>" << '\n';
    std::exit(EXIT_FAILURE);
  }

  const int rseed { std::stoi(argv[1]) };
  assert(rseed > 0);

  // collinear<double, Amp0q5g_a, Amp0q4g_a>(5, rseed, "data/collinear.5pt.f64." + std::to_string(rseed) + ".csv", true);
  // collinear<dd_real, Amp0q5g_a, Amp0q4g_a>(5, rseed, "data/collinear.5pt.f128." + std::to_string(rseed) + ".csv", true);
  // collinear<qd_real, Amp0q5g_a, Amp0q4g_a>(5, rseed, "data/collinear.5pt.f256." + std::to_string(rseed) + ".csv", true);

  // collinear<double, Amp0q6g, Amp0q5g_a>(6, rseed, "data/collinear.6pt.f64." + std::to_string(rseed) + ".csv", false);
  // collinear<dd_real, Amp0q6g, Amp0q5g_a>(6, rseed, "data/collinear.6pt.f128." + std::to_string(rseed) + ".csv", false);
  // collinear<qd_real, Amp0q6g, Amp0q5g_a>(6, rseed, "data/collinear.6pt.f256." + std::to_string(rseed) + ".csv", false);

  // collinear5_1Lsq<double>(rseed, "data/collinear.5pt.1L.f64." + std::to_string(rseed) + ".csv");
  // collinear5_1Lsq<dd_real>(rseed, "data/collinear.5pt.1L.f128." + std::to_string(rseed) + ".csv");
  // collinear5_1Lsq<qd_real>(rseed, "data/collinear.5pt.1L.f256." + std::to_string(rseed) + ".csv");
}
