#!/usr/bin/env python3

import argparse
import numpy
from soft_ps import mdot, test_physical


def get_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "multiplicity",
        nargs="+",
        type=int,
    )
    parser.add_argument(
        "-r",
        "--ps-index",
        type=int,
        default=0,
    )
    parser.add_argument(
        "-j",
        "--soft-index",
        type=int,
        default=3,
    )

    return parser.parse_args()


if __name__ == "__main__":
    args = get_args()
    j = args.soft_index
    d = 4

    for m in args.multiplicity:
        full = numpy.genfromtxt(f"data/soft.ps.{m}pt.{args.ps_index}.csv")

        n = len(full)
        # mm = m - 1

        full = numpy.reshape(full, (n, m, d))

        for i in range(m):
            for k in range(i + 1, m):
                if j in (i, k):
                    continue

                # red = numpy.empty((n, mm, 4))
                red = numpy.empty_like(full)

                for z, p in enumerate(full):
                    # r = numpy.copy(p)
                    for a in range(m):
                        if a == j:
                            red[z][a] = numpy.zeros(d)
                        elif a not in (i, k):
                            red[z][a] = p[a]

                    b = mdot(p[i], p[j]) / (mdot(p[i], p[k]) + mdot(p[j], p[k]))

                    # r[i] = p[i] + p[j] - b * p[k]
                    # r[k] = (1 + b) * p[k]
                    red[z][i] = p[i] + p[j] - b * p[k]
                    red[z][k] = (1 + b) * p[k]

                    # red[z] = r[[a for a in range(m) if a != j]]

                    test_physical(red[z])

                numpy.savetxt(
                    f"data/soft.ps.{m}pt.{args.ps_index}.reduced.i={i}.j={j}.k={k}.csv",
                    # numpy.reshape(red, (n, mm * d)),
                    numpy.reshape(red, (n, m * d)),
                )
