#!/usr/bin/env python3

import argparse, multiprocessing, math, random
import numpy


def mdot(a, b):
    return a[0] * b[0] - numpy.dot(a[1:], b[1:])


def test_physical(ps):
    for p in ps:
        assert numpy.isclose(mdot(p, p), 0)
    for s in numpy.sum(ps, axis=0):
        assert numpy.isclose(s, 0)


def getSoftPS(s_com, E_soft, n_out, i, j, k):
    p_in = 0.5 * numpy.array([[-1, 0, 0, 1], [-1, 0, 0, -1]])
    while True:
        try:
            u, r, v = makeSoftPS(E_soft, n_out, i, j, k)
            p_out = psFromURV(u, r, v)
            ps = s_com * numpy.vstack([p_in, p_out])
            test_physical(ps)
            break
        except AssertionError:
            pass
    return ps


def psFromURV(u, r, v):
    return numpy.array(
        [
            numpy.array(
                [
                    (uu * uu + rr * rr + vv * vv) / 2,
                    uu * rr,
                    uu * vv,
                    (uu * uu - rr * rr - vv * vv) / 2,
                ]
            )
            for uu, rr, vv in zip(u, r, v)
        ]
    )


def makeSoftPS(E_i, n_out, i, j, k):
    e_i = numpy.zeros(n_out)
    e_i[i - 1] = 1
    uhat, rhat, vhat = getURVhat(e_i)

    E_u = -100
    while E_u < 0:
        E_u, E_rv = math.sqrt(2 * E_i) * randomDirection(2)
        E_r, E_v = E_rv * randomDirection(2)
    assert numpy.isclose(0.5 * (E_u**2 + E_r**2 + E_v**2), E_i)

    uhj, uhk = uhat[j - 1], uhat[k - 1]
    ruhjk = math.hypot(uhj, uhk)
    assert ruhjk**2 - E_u**2 > 0

    rujk = math.sqrt(ruhjk**2 - E_u**2)
    theta_u = math.atan2(uhk, uhj) + random.random() * math.pi / 2 * E_i
    u = numpy.array(uhat)
    u[i - 1] = E_u
    u[j - 1] = rujk * math.cos(theta_u)
    u[k - 1] = rujk * math.sin(theta_u)
    assert numpy.isclose(numpy.sum(u**2), 1)

    def adaptrv(ref, Ei):
        refj, refk = ref[j - 1], ref[k - 1]
        rrefjk = math.hypot(refj, refk)
        assert rrefjk**2 - Ei**2 > 0

        rretjk = math.sqrt(rrefjk**2 - Ei**2)
        p = refj * uhj + refk * uhk
        a = 1 + u[k - 1] ** 2 / u[j - 1] ** 2
        b = -2 * u[k - 1] / u[j - 1] ** 2 * (p - Ei * E_u)
        c = (p - Ei * E_u) ** 2 / u[j - 1] ** 2 - rretjk**2
        assert b**2 - 4 * a * c > 0

        retk1 = (-b + math.sqrt(b**2 - 4 * a * c)) / (2 * a)
        retj1 = (p - retk1 * u[k - 1] - Ei * E_u) / u[j - 1]
        assert numpy.isclose(retj1 * u[j - 1] + retk1 * u[k - 1] + E_u * Ei, p)
        assert numpy.isclose(retj1**2 + retk1**2, rretjk**2)

        retk2 = (-b - math.sqrt(b**2 - 4 * a * c)) / (2 * a)
        retj2 = (p - retk2 * u[k - 1] - Ei * E_u) / u[j - 1]
        assert numpy.isclose(retj2 * u[j - 1] + retk2 * u[k - 1] + E_u * Ei, p)
        assert numpy.isclose(retj2**2 + retk2**2, rretjk**2)

        ret = numpy.array(ref)
        ret[i - 1] = Ei

        d1 = (retj1 - refj) ** 2 + (retk1 - refk) ** 2
        d2 = (retj2 - refj) ** 2 + (retk2 - refk) ** 2

        if d1 < d2:
            ret[j - 1] = retj1
            ret[k - 1] = retk1
        else:
            ret[j - 1] = retj2
            ret[k - 1] = retk2

        return ret

    r = adaptrv(rhat, E_r)
    v = adaptrv(vhat, E_v)

    return u, r, v


def getURVhat(e_i):
    positiveQuarter = False
    while not positiveQuarter:
        uhat = randomVectorInPerpSpace(e_i)
        positiveQuarter = numpy.all(uhat >= 0)

    lam = random.random()
    rhat = math.sqrt(lam) * randomVectorInPerpSpace([uhat, e_i])
    vhat = math.sqrt(1 - lam) * randomVectorInPerpSpace([uhat, e_i])

    assert numpy.isclose(numpy.dot(uhat, rhat), 0)
    assert numpy.isclose(numpy.dot(uhat, vhat), 0)
    assert numpy.isclose(numpy.dot(uhat, uhat), 1)
    assert numpy.isclose(numpy.dot(rhat, rhat) + numpy.dot(vhat, vhat), 1)

    return uhat, rhat, vhat


def randomVectorInPerpSpace(v):
    if isinstance(v, numpy.ndarray):
        ndim = v.shape[0]
        perpBasis = getBasis([v])
        nbasis = ndim - 1
        nfixed = 1
    elif isinstance(v, list):
        ndim = v[0].shape[0]
        # getBasis assumes input is already othonormal
        ortho = GramSchmidt(v)
        perpBasis = getBasis(ortho)
        nbasis = ndim - len(v)
        nfixed = len(v)
    perpBasis = numpy.array(perpBasis[nfixed:])
    return numpy.dot(randomDirection(nbasis), perpBasis)


def GramSchmidt(vs):
    newvs = []
    for v in vs:
        newv = numpy.copy(v)
        for other in newvs:
            newv -= numpy.dot(newv, other) * other
            assert numpy.isclose(numpy.dot(newv, other), 0)
        newv /= numpy.linalg.norm(newv)
        assert numpy.isclose(numpy.dot(newv, newv), 1)
        newvs.append(newv)
    return newvs


def getBasis(preexisting):
    ndim = len(preexisting[0])
    refs = numpy.eye(ndim)
    vs = list(preexisting)
    ind = 0
    while len(vs) < ndim:
        v = numpy.copy(refs[ind])
        ind += 1
        for ivb, vb in enumerate(vs):
            v -= numpy.dot(v, vb) * vb
            for vbb in vs[:ivb]:
                assert numpy.isclose(0, numpy.dot(v, vbb)), (numpy.dot(v, vbb), v, vbb)
        if numpy.allclose(v, numpy.zeros(ndim)):
            continue
        v /= math.sqrt(numpy.sum(numpy.square(v)))
        vs.append(v)
    return vs


def randomDirection(n):
    while True:
        p = numpy.random.uniform(-1, 1, n)
        r = numpy.sum(numpy.square(p))
        if r <= 1:
            break
    p /= math.sqrt(r)
    return p


def get_args():
    parser = argparse.ArgumentParser(
        description="Produce phase space points parametrised in a soft limit"
    )
    parser.add_argument(
        "-r",
        "--repeats",
        metavar="R",
        type=int,
        default=1,
        help="How many phase space paths to average over [default: 1]",
    )
    parser.add_argument(
        "-p",
        "--points",
        metavar="P",
        type=int,
        default=100,
        help="How many phase space points per path [default: 100]",
    )
    parser.add_argument(
        "-j",
        "--processes",
        metavar="N",
        type=int,
        default=1,
        help="Number of simultaneous processes to run [default: 1]",
    )
    parser.add_argument(
        "-m",
        "--multiplicities",
        metavar="M",
        type=int,
        nargs="+",
        default=[5, 6],
        help="Which multiplicities to run [default: 5 6]",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Turn on debug prints",
    )
    args = parser.parse_args()
    return args


def run(s_com, i, j, k, n, r, points, v):
    n_out = n - 2

    pss = numpy.array(
        [
            getSoftPS(s_com, E_i, n_out, i, j, k)
            for E_i in numpy.logspace(-0.5, -8, points)
        ]
    )

    if v:
        for ps in pss:
            print(ps[1 + i][0])

    filename = f"data/soft.ps.{n}pt.{r}.csv"

    print(f"Writing {filename}")

    numpy.savetxt(filename, [ps.flatten() for ps in pss])


if __name__ == "__main__":
    args = get_args()

    s = 1

    i = 2  # i is soft, unity indexed outgoing leg
    j = 3
    k = 1

    with multiprocessing.Pool(processes=args.processes) as pool:
        for n in args.multiplicities:
            for r in range(args.repeats):
                pool.apply_async(run, (s, i, j, k, n, r, args.points, args.verbose))

        pool.close()
        pool.join()
