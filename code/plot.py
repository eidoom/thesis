#!/usr/bin/env python3

import argparse, pathlib, operator, math
import numpy, matplotlib.pyplot, matplotlib.cm

PHI = (1 + math.sqrt(5)) / 2


def mdot(a, b):
    return a[0] * b[0] - numpy.dot(a[1:], b[1:])


def get_args():
    parser = argparse.ArgumentParser(
        description="Produce plots showing scaling behaviour of factorised expressions in infrared limits"
    )

    parser.add_argument(
        "-r",
        "--refresh-cache",
        action="store_true",
        help="Refresh all caches",
    )
    parser.add_argument(
        "-p",
        "--repeat-paths",
        action="store_true",
        help="Average over repeated phase space paths [only supported for soft plots with single line]",
    )

    args = parser.parse_args()

    if (
        args.refresh_cache
        and not input("Are you sure you want to refresh the cache? [y] ") == "y"
    ):
        exit()

    return args


def load(filename, refresh_cache=False):
    cache = f"{filename}.npy"

    if refresh_cache or not pathlib.Path(cache).exists():
        data = numpy.genfromtxt(filename)
        numpy.save(cache, data)
        return data

    return numpy.load(cache, allow_pickle=True)


def plot_coll_tree(refresh_cache=False):
    fig, ax = matplotlib.pyplot.subplots()

    muls = (5, 6)
    labels = (r"$gg\to ggg$", r"$gg\to gggg$")
    colours = numpy.array_split(
        matplotlib.cm.tab20(range(2 * len(labels))), len(labels)
    )

    styles = ("dotted", "dashed", "solid")
    precs = ("f64", "f128", "f256")

    for mul, label, (c1, c0) in zip(muls, labels, colours):
        for prec, style in zip(precs, styles):
            s23s, vals, diags, corrs, *_ = numpy.transpose(
                load(f"data/collinear.{mul}pt.{prec}.1.csv", refresh_cache)
            )

            ax.plot(
                numpy.sqrt(s23s),
                numpy.abs(1 - (diags / vals)),
                # diags / vals,
                label=label + r" LS $\texttt{" + prec + "}$",
                linestyle=style,
                color=c0,
            )
            ax.plot(
                numpy.sqrt(s23s),
                numpy.abs(1 - ((diags + corrs) / vals)),
                # (diags + corrs) / vals,
                label=label + r" FS $\texttt{" + prec + "}$",
                linestyle=style,
                color=c1,
            )

    ax.set_xscale("log", base=10)
    ax.set_yscale("log", base=10)

    ax.set_xlabel(r"$\sqrt{s_{34}/s_{12}}$")
    ax.set_ylabel(r"$|\mathcal{D}_\parallel|$")

    ax.legend(ncol=len(labels), loc="upper center", bbox_to_anchor=(0.5, -0.15))

    fig.savefig(f"collinear-scaling.0L.pdf", bbox_inches="tight")


def plot_coll_loop(
    comparison="rd", catch_end=False, cut=0.5, start=0, refresh_cache=False
):
    fig, ax = matplotlib.pyplot.subplots()

    comp_labels = ("$m_{-2}$", "$m_{-1}$", "$m_0$")
    spin_labels = ("LS", "FS")
    colours = numpy.flip(
        numpy.array_split(
            matplotlib.cm.tab20(range(len(spin_labels) * len(comp_labels))),
            len(comp_labels),
        ),
        axis=1,
    )

    styles = ("dotted", "dashed", "solid")
    precs = ("f64", "f128", "f256")

    data = {}
    for prec in precs:
        s23s, *vals = numpy.transpose(
            load(f"data/collinear.5pt.{prec}.1.csv", refresh_cache)
        )
        parts = numpy.array_split(numpy.array(vals)[:, start:], 4)

        data[prec] = (s23s, parts[1:])

    for i, (comp_label, colour_pair) in enumerate(zip(comp_labels, colours)):
        for prec, style in zip(precs, styles):
            s23s, parts = data[prec]
            vals, diags, corrs = parts[i]

            r_ls = diags / vals
            r_fs = (diags + corrs) / vals

            rds_ls = numpy.abs(1 - r_ls)
            rds_fs = numpy.abs(1 - r_fs)

            if catch_end:
                try:
                    end = (
                        next(
                            (
                                i
                                for i, p in enumerate(rds_fs)
                                if i > 5 and abs(p - 1) < cut
                            )
                        )
                        + 1
                    )
                except StopIteration:
                    end = len(rds_fs)
            else:
                end = len(rds_fs)

            s23s = s23s[:end]

            if comparison == "rd":
                vals = numpy.array([rds_ls, rds_fs])
            elif comparison == "r":
                vals = numpy.abs([r_ls, r_fs])

            for v, colour, spin_label in zip(vals, colour_pair, spin_labels):
                v = v[:end]

                ax.plot(
                    numpy.sqrt(s23s),
                    v,
                    label=f"{comp_label} {spin_label} $\\texttt{{{prec}}}$",
                    linestyle=style,
                    color=colour,
                )

    ax.set_xscale("log", base=10)
    ax.set_yscale("log", base=10)

    ax.set_xlabel(r"$\sqrt{s_{34}/s_{12}}$")

    if comparison == "rd":
        ax.set_ylabel(r"$|\mathcal{D}_\parallel|$")
    elif comparison == "r":
        ax.set_ylabel(r"$|\mathcal{R}_\parallel|$")

    # ax.set_ylim(8e-11, 8e-1)

    ax.legend(ncol=len(comp_labels), loc="upper center", bbox_to_anchor=(0.5, -0.15))

    fig.savefig(f"collinear-scaling.1L.{comparison}.pdf", bbox_inches="tight")


# def average(mul, prec, refresh_cache=False):
#     vss = numpy.array(
#         [
#             load(filename, refresh_cache)
#             for filename in pathlib.Path("data").glob(f"soft.{mul}pt.{prec}.*.csv")
#         ]
#     )
#     return numpy.mean(vss, axis=0)


def plot_soft_together(refresh_cache=False):
    fig, ax = matplotlib.pyplot.subplots()

    muls = (5, 6)
    labels = (r"$gg\to ggg$", r"$gg\to gggg$")
    colours = matplotlib.cm.tab10(range(len(labels)))
    j = 3  # soft leg (zero index)
    rseed = 0
    styles = ("dotted", "dashed", "solid")
    precs = ("f64", "f128", "f256")

    for mul, label, colour in zip(muls, labels, colours):
        # if mul == 6:
        #     continue
        ehs = numpy.array(
            [
                p[4 * j] / math.sqrt(2.0 * mdot(p[:4], p[4:8]))
                for p in load(f"data/soft.ps.{mul}pt.{rseed}.csv", refresh_cache)
            ]
        )

        for prec, style in zip(precs, styles):
            # v = average(mul, prec, refresh_cache)
            v = load(f"data/soft.{mul}pt.{prec}.{rseed}.csv", refresh_cache)

            ax.plot(
                ehs,
                numpy.abs(1 - v[:, 1] / v[:, 0]),
                label=f"{label} $\\texttt{{{prec}}}$",
                linestyle=style,
                color=colour,
            )

    ax.set_xscale("log", base=10)
    ax.set_yscale("log", base=10)

    ax.set_xlabel(r"${E_{" + str(j + 1) + r"}}/\sqrt{s_{12}}$")
    ax.set_ylabel(r"$|\mathcal{D}_s|$")

    ax.legend(ncol=len(labels))

    fig.savefig(f"soft-scaling.together.pdf", bbox_inches="tight")


def spin5(refresh_cache=False):
    data = load("data/collinear.5pt.f256.1.csv", refresh_cache)
    return numpy.array([coll / diag for diag, coll in data[:, [10, 11]]])


if __name__ == "__main__":
    args = get_args()

    matplotlib.pyplot.rc("text", usetex=True)
    matplotlib.pyplot.rc("font", family="serif")

    # print(spin5())

    # plot_coll_tree(args.refresh_cache)

    # plot_coll_loop(refresh_cache=args.refresh_cache)

    # plot_coll_loop(
    #     "r", catch_end=True, cut=0.9, refresh_cache=args.refresh_cache
    # )

    plot_soft_together(args.refresh_cache)
