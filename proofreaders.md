# Proofreading

Thank you very much for offering to proofread my thesis!
The TeX source is hosted in a Git repo at <https://gitlab.com/eidoom/thesis>.
The latest version of the PDF is available at <https://eidoom.gitlab.io/thesis/thesis.pdf>.
Please provide feedback by creating a new "Issue" at <https://gitlab.com/eidoom/thesis/-/issues>, which requires a GitLab account.
Or tell me another way, if you'd rather.

Ryan
