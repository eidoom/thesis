#include <algorithm>
#include <array>
#include <cmath>
#include <complex>
#include <iomanip>
#include <iostream>

#include "analytic/0q4g-analytic.h"
#include "analytic/0q5g-analytic.h"
#include "ir/split_g2gg-analytic.h"
#include "ngluon2/Mom.h"

int main()
{
    // 2 || 3 collinear massless 5-particle phase space (0 indexing)
    const std::array<MOM<double>, 5> full_mom { {
        { -5.0000000000000000e-01, 0.0000000000000000e+00, 0.0000000000000000e+00, 5.0000000000000000e-01 },
        { 4.9999949999999999e-01, 4.3301226887951738e-01, 0.0000000000000000e+00, 2.4999975000000005e-01 },
        { 2.9999999999999999e-01, -2.5960910680884997e-01, 2.8795483728213630e-04, -1.5034303689869646e-01 },
        { 2.0000050000000008e-01, -1.7340316207066742e-01, -2.8795483728213630e-04, -9.9656713101303585e-02 },
        { -5.0000000000000000e-01, 0.0000000000000000e+00, 0.0000000000000000e+00, -5.0000000000000000e-01 },
    } };

    // Catani-Seymour momentum mapping for 2 || 3 (0 indexing)
    const MOM<double> p { full_mom[2] + full_mom[3] };
    const double x { dot(full_mom[2], full_mom[3]) / dot(p, full_mom[4]) };

    // reduced phase space
    std::array<MOM<double>, 4> reduced_mom {};
    std::copy_n(full_mom.cbegin(), 2, reduced_mom.begin());
    reduced_mom[2] = p - x * full_mom[4];
    reduced_mom[3] = (1. + x) * full_mom[4];

    // momenta for splitting amplitude
    const std::array<MOM<double>, 3> splitting_mom { { -reduced_mom[2], full_mom[2], full_mom[3] } };

    // initialise amplitudes
    Amp0q5g_a<double> full_amp;
    Amp0q4g_a<double> reduced_amp;
    Splitg2gg_a<double> splitting_amp;

    // set momenta
    full_amp.setMomenta(full_mom.data());
    reduced_amp.setMomenta(reduced_mom.data());
    // the second argument is a reference momentum
    splitting_amp.setMomenta(splitting_mom.data(), reduced_mom[3]);

    // helicities for amplitudes
    const std::array<int, 5> full_hels { +1, +1, +1, -1, -1 };
    // the helicity values of the correlated legs do not matter
    const std::array<int, 4> reduced_hels { +1, +1, 0, -1 };
    const std::array<int, 3> splitting_hels { 0, +1, -1 };

    // initialise (flattened) 2x2 spin matrices
    // order: ++ +- -+ --
    std::array<std::complex<double>, 4> reduced_spn_mat {}, splitting_spn_mat {};

    // compute spin matrices
    // the first argument is the index of the correlated leg
    reduced_amp.born_spnmatrix(2, reduced_hels.data(), reduced_spn_mat.data());
    splitting_amp.born_spnmatrix(0, splitting_hels.data(), splitting_spn_mat.data());

    // while the final result will be real, the intermediate steps may be complex
    std::complex<double> limit {};

    // compute factorised matrix element including spin correlations
    for (int i { 0 }; i < 4; ++i) {
        limit += reduced_spn_mat[i] * splitting_spn_mat[3 - i];
    }

    // compute full matrix element
    const double amp { full_amp.born(full_hels.data()) };

    // print results (with unity indexing)
    std::cout
        << std::setprecision(1) << std::scientific
        << '\n'
        << std::setw(25) << "|s_{34}/s_{12}| = "
        << std::setw(7) << std::abs(dot(full_mom[2], full_mom[3]) / dot(full_mom[0], full_mom[1]))
        << '\n'
        << std::setw(25) << "|(amp^2-lim^2)/amp^2| = "
        << std::setw(7) << std::abs((amp - limit.real()) / amp)
        << '\n'
        << '\n';
}
